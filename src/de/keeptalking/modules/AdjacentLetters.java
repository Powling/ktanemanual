package de.keeptalking.modules;

import de.keeptalking.main.bomb.Module;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.*;

public class AdjacentLetters extends Module {

	private String first = "";
	private String second = "";
	private String third = "";

	private TextArea textArea;
	private Map<Character, String> map = new HashMap<>();

	public AdjacentLetters() {
		super("Adjacent Letters", "adjacent_letters");
		map.put('A', "GJMOY-HKPRW");
		map.put('B', "IKLRT-CDFYZ");
		map.put('C', "BHIJW-DEMTU");
		map.put('D', "IKOPQ-CJTUW");
		map.put('E', "ACGIJ-KSUWZ");
		map.put('F', "CERVY-AGJPQ");
		map.put('G', "ACFNS-HOQYZ");
		map.put('H', "LRTUX-DKMPS");
		map.put('I', "DLOWZ-EFNUV");
		map.put('J', "BQTUW-EHIOS");
		map.put('K', "AFPXY-DIORZ");
		map.put('L', "GKPTZ-ABRVX");
		map.put('M', "EILQT-BFPWX");
		map.put('N', "PQRSV-AFGHL");
		map.put('O', "HJLUZ-IQSTX");
		map.put('P', "DMNOX-CFHKR");
		map.put('Q', "CEOPV-BDIKN");
		map.put('R', "AEGSU-BNOXY");
		map.put('S', "ABEKQ-GMVYZ");
		map.put('T', "GVXYZ-CJLSU");
		map.put('U', "FMVXZ-BILNY");
		map.put('V', "DHMNW-AEJQX");
		map.put('W', "DFHMN-GLQRT");
		map.put('X', "BDFKW-AJNOV");
		map.put('Y', "BCHSU-EGMTW");
		map.put('Z', "JNRSY-CLMPV");
	}

	@Override
	public void update() {
		if(first.length() + second.length() + third.length() == 4*3) {
			char[] first = this.first.toCharArray();
			char[] second = this.second.toCharArray();
			char[] third = this.third.toCharArray();

			char[][] all = new char[3][4];

			System.arraycopy(first, 0, all[0], 0, 4);
			System.arraycopy(second, 0, all[1], 0, 4);
			System.arraycopy(third, 0, all[2], 0, 4);

			StringBuilder end = new StringBuilder();

			for (int y = 0; y < 3; y++) {
				for (int x = 0; x < 4; x++) {
					char current = all[y][x];
					String[] key = map.get(current).split("-");
					List<String> column = new ArrayList<>();
					List<String> row = new ArrayList<>();

					for (char c : key[0].toCharArray()) {
						column.add(String.valueOf(c));
					}
					for (char c : key[1].toCharArray()) {
						row.add(String.valueOf(c));
					}

					String up = "";
					String right = "";
					String down = "";
					String left = "";

					if (x > 0) {
						left = String.valueOf(all[y][x-1]);
					}
					if (x < 3) {
						right = String.valueOf(all[y][x+1]);
					}
					if (y > 0) {
						up = String.valueOf(all[y-1][x]);
					}
					if (y < 2) {
						down = String.valueOf(all[y+1][x]);
					}

					if (column.contains(left) || column.contains(right) || row.contains(up) || row.contains(down)) {
						end.append(current);
					}

				}
			}

			textArea.setText("Buttons to press:\n" + end);
		}
	}

	@Override
	public void loadFXML(ObservableList<Node> nodes, int nodect) {
		TextField firstField = (TextField) nodes.get(++nodect);
		TextField secondField = (TextField) nodes.get(++nodect);
		TextField thirdField = (TextField) nodes.get(++nodect);
		textArea = (TextArea) nodes.get(++nodect);

		firstField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 4) {
				firstField.setText(oldValue);
				return;
			}
			for (char c : newValue.toCharArray()) {
				if (!Character.isAlphabetic(c)) {
					firstField.setText(oldValue);
					return;
				}
			}
			firstField.setText(newValue.toUpperCase());
			first = newValue.toUpperCase();
			update();
		});

		secondField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 4) {
				secondField.setText(oldValue);
				return;
			}
			for (char c : newValue.toCharArray()) {
				if (!Character.isAlphabetic(c)) {
					secondField.setText(oldValue);
					return;
				}
			}

			secondField.setText(newValue.toUpperCase());
			second = newValue.toUpperCase();
			update();
		});

		thirdField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 4) {
				thirdField.setText(oldValue);
				return;
			}
			for (char c : newValue.toCharArray()) {
				if (!Character.isAlphabetic(c)) {
					thirdField.setText(oldValue);
					return;
				}
			}
			thirdField.setText(newValue.toUpperCase());
			third = newValue.toUpperCase();
			update();
		});

		super.loadFXML(nodes, nodect);
	}
}
