package de.keeptalking.modules;

import de.keeptalking.main.bomb.Indicator;
import de.keeptalking.main.bomb.Module;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import static de.keeptalking.modules.Laundry.Dry.*;
import static de.keeptalking.modules.Laundry.Iron.*;
import static de.keeptalking.modules.Laundry.Special.*;
import static de.keeptalking.modules.Laundry.Washing.*;

public class Laundry extends Module {

	private TextArea textArea;
	private ImageView leftImage;
	private ImageView rightImage;

	private static final Attribute corset = new Attribute(0, "Corset", C60, DRY_FLAT, F300, BLEACH);
	private static final Attribute shirt = new Attribute(1, "Shirt", C40, HIGH_HEAT, NO_STEAM, NO_TETRACHLORE);
	private static final Attribute skirt = new Attribute(2, "Skirt", C30, HANG_TO_DRY, IRON, REDUCED_MOISTURE);
	private static final Attribute skort = new Attribute(3, "Skort", MACHINE_WASH_GENTLE_DELICATE, TUMBLE_DRY, C200, REDUCED_MOISTURE);
	private static final Attribute shorts = new Attribute(4, "Shorts", DO_NOT_WRING, SHADE, F300, DO_NOT_BLEACH);
	private static final Attribute scarf = new Attribute(5, "Scarf", C95, DRY, C110, DO_NOT_DRYCLEAN);

	private static final Attribute polyester = new Attribute(0, "Polyester", C50, NO_HEAT, F300, PETROLEUM_ONLY);
	private static final Attribute cotton = new Attribute(1, "Cotton", C95, MEDIUM_HEAT, IRON, DO_NOT_DRYCLEAN);
	private static final Attribute wool = new Attribute(2, "Wool", HANDWASH, SHADE, C200, REDUCED_MOISTURE);
	private static final Attribute nylon = new Attribute(3, "Nylon", C30, DRIP_DRY, NO_STEAM, Special.LOW_HEAT);
	private static final Attribute corduroy = new Attribute(4, "Corduroy", C40, DRIP_DRY, C110, WET_CLEANING);
	private static final Attribute leather = new Attribute(5, "Leather", DO_NOT_WASH, DO_NOT_DRY, DO_NOT_IRON, NO_TETRACHLORE);

	private static final Attribute ruby = new Attribute(0, "Ruby Fountain", C60, HIGH_HEAT, DO_NOT_IRON, ANY_SOLVENT);
	private static final Attribute star = new Attribute(1, "Star Lemon Quartz", C95, DRY_FLAT, IRON, Special.LOW_HEAT);
	private static final Attribute sapphire = new Attribute(2, "Sapphire Springs", C30, TUMBLE_DRY, C200, SHORT_CYCLE);
	private static final Attribute jade = new Attribute(3, "Jade Cluster", C30, DO_NOT_TUMBLE_DRY, F300, NO_STEAM_FINISH);
	private static final Attribute clouded = new Attribute(4, "Clouded Pearl", MACHINE_WASH_PERMANENT_PRESS, Dry.LOW_HEAT, NO_STEAM, Special.LOW_HEAT);
	private static final Attribute malinite = new Attribute(5, "Malinite", C60, MEDIUM_HEAT, C200, NO_CHLORINE);

	public Laundry() {
		super("Laundry", "laundry");
	}

	@Override
	public void update() {
		int solved = bomb.getSolvedModules();
		int unsolved = bomb.getUnsolvedModules();

		if (bomb.getBatteries() == 4 && bomb.getHolders() == 2 && bomb.getLit().contains(Indicator.BOB)) {
			textArea.setText("Just put that coin in.");
			return;
		}

		int itemID = unsolved + bomb.getLit().size() + bomb.getUnlit().size();
		int materialID = bomb.getPorts().size() + solved - bomb.getHolders();
		int colorID = bomb.serialLastDigit() + bomb.getBatteries();
		
		while (itemID > 5) itemID -= 6;
		while (itemID < 0) itemID += 6;
		while (materialID > 11) materialID -= 6;
		while (materialID < 6) materialID += 6;
		while (colorID > 17) colorID -= 6;
		while (colorID < 12) colorID += 6;
		
		Attribute item = getByID(itemID);
		Attribute material = getByID(materialID);
		Attribute color = getByID(colorID);

		Washing washing = null;
		Dry dry = null;
		Iron iron = null;
		Special special = null;

		if (bomb.serialCharMatches(material.name.toCharArray())) {
			special = color.special;
		}
		if (material == wool || color == star) {
			dry = HIGH_HEAT;
		}
		if (item == corset || material == corduroy) {
			special = material.special;
		}
		if (material == leather || color == jade) {
			washing = C30;
		}
		if (color == clouded) {
			special = NO_CHLORINE;
		}

		if (washing == null) {
			washing = material.washing;
		}
		if (dry == null) {
			dry = color.dry;
		}
		if (special == null) {
			special = item.special;
		}
		iron = item.iron;

		instance.getImageLoader().setImage(leftImage, "laundry/" + washing);
		instance.getImageLoader().setImage(rightImage, "laundry/" + dry);
		textArea.setText(String.format("Ironing: %s \nSpecial Instruction: %s", iron, special));

	}

	@Override
	public void loadFXML(ObservableList<Node> nodes, int nodect) {
		textArea = (TextArea) nodes.get(++nodect);
		leftImage = (ImageView) nodes.get(++nodect);
		rightImage = (ImageView) nodes.get(++nodect);

		super.loadFXML(nodes, nodect);
	}

	public Attribute getByID(int id) {
		if (id >= 12) {
			id -= 12;
			switch (id) {
				case 0:
					return ruby;
				case 1:
					return star;
				case 2:
					return sapphire;
				case 3:
					return jade;
				case 4:
					return clouded;
				case 5:
					return malinite;
			}
		} else if (id >= 6){
			id -= 6;
			switch (id) {
				case 0:
					return polyester;
				case 1:
					return cotton;
				case 2:
					return wool;
				case 3:
					return nylon;
				case 4:
					return corduroy;
				case 5:
					return leather;
			}
		} else {
			switch (id) {
				case 0:
					return corset;
				case 1:
					return shirt;
				case 2:
					return skirt;
				case 3:
					return skort;
				case 4:
					return shorts;
				case 5:
					return scarf;
			}
		}
		return null;
	}

	private static class Attribute {
		int id;
		String name;
		Washing washing;
		Dry dry;
		Iron iron;
		Special special;

		public Attribute(int id, String name, Washing washing, Dry dry, Iron iron, Special special) {
			this.id = id;
			this.name = name;
			this.washing = washing;
			this.dry = dry;
			this.iron = iron;
			this.special = special;
		}
	}

	enum Washing {
		C30,
		C40,
		C50,
		C60,
		C70,
		C95,
		DO_NOT_WRING,
		MACHINE_WASH_PERMANENT_PRESS,
		MACHINE_WASH_GENTLE_DELICATE,
		HANDWASH,
		DO_NOT_WASH
	}

	enum Dry {
		TUMBLE_DRY,
		LOW_HEAT,
		MEDIUM_HEAT,
		HIGH_HEAT,
		NO_HEAT,
		HANG_TO_DRY,
		DRIP_DRY,
		DRY_FLAT,
		SHADE,
		DO_NOT_DRY,
		DO_NOT_TUMBLE_DRY,
		DRY
	}

	enum Iron {
		IRON,
		DO_NOT_IRON,
		C110,
		F300,
		C200,
		NO_STEAM,
	}

	enum Special {
		BLEACH,
		DO_NOT_BLEACH,
		NO_CHLORINE,
		DRYCLEAN,
		ANY_SOLVENT,
		NO_TETRACHLORE,
		PETROLEUM_ONLY,
		WET_CLEANING,
		DO_NOT_DRYCLEAN,
		SHORT_CYCLE,
		REDUCED_MOISTURE,
		LOW_HEAT,
		NO_STEAM_FINISH
	}

}
