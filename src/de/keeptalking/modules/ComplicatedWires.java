package de.keeptalking.modules;

import de.keeptalking.main.bomb.Module;
import de.keeptalking.main.bomb.Port;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;

public class ComplicatedWires extends Module {

	private boolean red = false;
	private boolean blue = false;
	private boolean led = false;
	private boolean star = false;

	private TextArea textArea;

	public ComplicatedWires() {
		super("Complicated Wires", "complicated_wires");
	}

	@Override
	public void update() {
		if (red) {
			if (blue) {
				if (led) {
					if (star) {
						d();
					} else {
						s();
					}
				} else {
					if (star) {
						p();
					} else {
						s();
					}
				}
			} else {
				if (led) {
					b();
				} else {
					if (star) {
						c();
					} else {
						s();
					}
				}
			}
		} else {
			if (blue) {
				if (led) {
					p();
				} else {
					if (star) {
						d();
					} else {
						s();
					}
				}
			} else {
				if (led) {
					if (star) {
						b();
					} else {
						d();
					}
				} else {
					c();
				}
			}
		}
	}

	private void s() {
		display(bomb.serialLastDigit() % 2 == 0);
	}

	private void p() {
		display(bomb.hasPort(Port.Parallel));
	}

	private void b() {
		display(bomb.getBatteries() >= 2);
	}

	private void c() {
		display(true);
	}

	private void d() {
		display(false);
	}

	private void display(boolean cut) {
		textArea.setText(cut ? "Cut the wire." : "Leave the wire.");
	}

	@Override
	public void loadFXML(ObservableList<Node> nodes, int nodect) {
		Button clear = (Button) nodes.get(++nodect);
		CheckBox blueBox = (CheckBox) nodes.get(++nodect);
		CheckBox redBox = (CheckBox) nodes.get(++nodect);
		CheckBox ledBox = (CheckBox) nodes.get(++nodect);
		CheckBox starBox = (CheckBox) nodes.get(++nodect);
		textArea = (TextArea) nodes.get(++nodect);
		Button inverse = (Button) nodes.get(++nodect);

		redBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
			red = newValue;
			update();
		});

		blueBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
			blue = newValue;
			update();
		});

		ledBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
			led = newValue;
			update();
		});

		starBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
			star = newValue;
			update();
		});

		clear.setOnAction(event -> {
			red = false;
			blue = false;
			led = false;
			star = false;
			blueBox.setSelected(false);
			redBox.setSelected(false);
			ledBox.setSelected(false);
			starBox.setSelected(false);
		});

		inverse.setOnAction(event -> {
			red = !red;
			blue = !blue;
			led = !led;
			star = !star;
			blueBox.setSelected(blue);
			redBox.setSelected(red);
			ledBox.setSelected(led);
			starBox.setSelected(star);
		});

		super.loadFXML(nodes, nodect);
	}
}
