package de.keeptalking.modules;

import de.keeptalking.main.bomb.Indicator;
import de.keeptalking.main.bomb.Module;
import de.keeptalking.main.bomb.specific.Color;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import javax.xml.soap.Text;

import static de.keeptalking.main.bomb.specific.Color.*;

public class TheButton extends Module {

	private String label = "";
	private Color color;
	private TextField textField;

	public TheButton() {
		super("The Button", "the_button");
	}

	@Override
	public void update() {
		if (!label.isEmpty() && !(color == null)) {
			if (color == BLUE && label.equals("Abort")) {
				textField.setText("Hold the button");

			} else if (bomb.getBatteries() > 1 && label.equals("Detonate")) {
				textField.setText("Press and immediately release the button");

			} else if (color == WHITE && bomb.getLit().contains(Indicator.CAR)) {
				textField.setText("Hold the button");

			} else if (bomb.getBatteries() > 2 && bomb.getLit().contains(Indicator.FRK)) {
				textField.setText("Press and immediately release the button");

			} else if (color == YELLOW) {
				textField.setText("Hold the button");

			} else if (color == RED && label.equals("Hold")) {
				textField.setText("Press and immediately release the button");

			} else {
				textField.setText("Hold the button");

			}
		} else {
			textField.setText("");
		}
	}

	@Override
	public void loadFXML(ObservableList<Node> nodes, int nodect) {
		ComboBox colorBox = (ComboBox) nodes.get(++nodect);
		ComboBox labelBox = (ComboBox) nodes.get(++nodect);
		textField = (TextField) nodes.get(++nodect);

		labelBox.getItems().clear();
		colorBox.getItems().clear();
		labelBox.getItems().addAll("Press", "Hold", "Detonate", "Abort");
		colorBox.getItems().addAll(RED, BLUE, YELLOW, WHITE);

		labelBox.valueProperty().addListener((observable, oldValue, newValue) -> {
			label = newValue.toString();
			update();
		});
		colorBox.valueProperty().addListener((observable, oldValue, newValue) -> {
			color = (Color) newValue;
			update();
		});

		super.loadFXML(nodes, nodect);
	}
}
