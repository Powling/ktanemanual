package de.keeptalking.main.bomb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PortPlate {

	private List<Port> ports;

	public PortPlate(Port... ports) {
		this.ports = new ArrayList<>(Arrays.asList(ports));
	}

	public boolean contains(Port port) {
		return ports.contains(port);
	}

	public List<Port> getPorts() {
		return ports;
	}
}
