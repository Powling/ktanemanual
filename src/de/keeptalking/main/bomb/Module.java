package de.keeptalking.main.bomb;

import de.keeptalking.main.KTaNEMain;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;

import java.io.IOException;

public abstract class Module {

	protected KTaNEMain instance = KTaNEMain.instance;
	protected Bomb bomb = instance.bomb;

	private String name;
	private String fxml;

	public Module(String name, String fxml) {
		this.name = name;
		this.fxml = fxml;
	}

	public String getName() {
		return name;
	}

	public String getFxml() {
		return fxml;
	}

	public void openScene() {
		bomb = instance.bomb;
		try {
			int nodect = -1;
			Parent root = FXMLLoader.load(getClass().getResource("/de/keeptalking/modules/" + fxml + ".fxml"));

			((Button) root.getChildrenUnmodifiable().get(++nodect)).setOnAction(event -> instance.chooseModule());
			((Button) root.getChildrenUnmodifiable().get(++nodect)).setOnAction(event -> {
				bomb.addSolved();
				update();
			});
			((Button) root.getChildrenUnmodifiable().get(++nodect)).setOnAction(event -> {
				bomb.addStrike();
				update();
			});

			loadFXML(root.getChildrenUnmodifiable(), nodect);
			instance.setScene(root);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public abstract void update();

	public void loadFXML(ObservableList<Node> nodes, int nodect) {
		update();
	}

	@Override
	public String toString() {
		return name;
	}
}
