package de.keeptalking.main.bomb;

import de.keeptalking.main.KTaNEMain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Bomb {

	private String serial = "";
	private int batteries;
	private int holders;
	private int plates;
	private int empties;
	private Set<Indicator> lit = new HashSet<>();
	private Set<Indicator> unlit = new HashSet<>();
	private List<Port> ports = new ArrayList<>();
	private List<Module> modules = new ArrayList<>();

	private int totalModules;
	private int solvedModules;
	private int strikes;

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public int serialLastDigit() {
		int num = -1;

		if(serial == null) return num;

		for (char c : serial.toCharArray()) {
			if (Character.isDigit(c)) {
				num = (int) c;
			}
		}
		return num;
	}

	public boolean serialCharMatches(char... chars) {
		for (char c : chars) {
			c = Character.toUpperCase(c);

			if (serial.contains(Character.toString(c))) {
				return true;
			}
		}
		return false;
	}

	public boolean indicatorIsLit(Indicator ind) {
		return lit.contains(ind);
	}

	public boolean indicatorIsUnlit(Indicator ind) {
		return unlit.contains(ind);
	}

	public boolean indicatorIsThere(Indicator ind) {
		return !indicatorIsLit(ind) && !indicatorIsUnlit(ind);
	}

	public int portCountOf(Port port) {
		int count = 0;
		for (Port p : ports) {
			if (p == port) {
				count++;
			}
		}
		return count;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void addModule(Module module) {
		modules.add(module);
	}

	public void addLit(Indicator lit) {
		this.lit.add(lit);
	}

	public int getEmpties() {
		return empties;
	}

	public void setEmpties(int empties) {
		this.empties = empties;
	}

	public int getPlates() {
		return plates;
	}

	public int getUnsolvedModules() {
		return totalModules - solvedModules;
	}

	public void setPlates(int plates) {
		this.plates = plates;
	}

	public void addUnlit(Indicator unlit) {
		this.unlit.add(unlit);
	}

	public void addPort(Port port) {
		ports.add(port);
	}

	public boolean hasPort(Port port) {
		return ports.contains(port);
	}

	public int getBatteries() {
		return batteries;
	}

	public void setBatteries(int batteries) {
		this.batteries = batteries;
	}

	public Set<Indicator> getLit() {
		return lit;
	}

	public Set<Indicator> getUnlit() {
		return unlit;
	}

	public List<Port> getPorts() {
		return ports;
	}

	public int getHolders() {
		return holders;
	}

	public void addSolved() {
		solvedModules++;

		if (solvedModules == totalModules) {
			KTaNEMain.instance.chooseModule();
		}
	}

	public void addStrike() {
		strikes++;
	}

	public void setHolders(int holders) {
		this.holders = holders;
	}

	public void setStrikes(int strikes) {
		this.strikes = strikes;
	}

	public void setSolvedModules(int solvedModules) {
		this.solvedModules = solvedModules;
	}

	public void setTotalModules(int totalModules) {
		this.totalModules = totalModules;
	}

	public int getSolvedModules() {
		return solvedModules;
	}

	public int getStrikes() {
		return strikes;
	}

	public int getTotalModules() {
		return totalModules;
	}
}
