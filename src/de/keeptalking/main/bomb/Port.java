package de.keeptalking.main.bomb;

public enum Port {

	DVI,
	Parallel,
	PS2,
	RJ45,
	Serial,
	RCA

}
