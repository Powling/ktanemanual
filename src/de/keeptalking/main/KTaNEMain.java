package de.keeptalking.main;

import de.keeptalking.main.bomb.Bomb;
import de.keeptalking.util.ImageLoader;
import de.keeptalking.util.ModuleLoader;
import de.keeptalking.util.SceneLoader;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

public class KTaNEMain extends Application {

	public static KTaNEMain instance;

	private Stage stage;
	public Bomb bomb;

	private HashMap<String, Parent> scenes = new HashMap<>();
	private SceneLoader sceneLoader;
	private ModuleLoader moduleLoader;
	private ImageLoader imageLoader;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage ps) {
		long time = System.currentTimeMillis();
		instance = this;
		stage = ps;
		bomb = new Bomb();
		sceneLoader = new SceneLoader();
		imageLoader = new ImageLoader();
		moduleLoader = new ModuleLoader();

		try {
			sceneLoader.loadCreateBomb(false);

			moduleLoader.registerModules();
			sceneLoader.loadChooseModule();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Finished loading in " + (System.currentTimeMillis() - time) + " milliseconds.");

		stage.setScene(new Scene(new Pane()));
		setScene("create_bomb");
	}

	public ModuleLoader getModuleLoader() {
		return moduleLoader;
	}

	public ImageLoader getImageLoader() {
		return imageLoader;
	}

	public HashMap<String, Parent> getScenes() {
		return scenes;
	}

	public void setScene(String name) {
		Parent root = scenes.get(name);
		stage.getScene().setRoot(root);

		stage.show();
	}
	public void setScene(Parent root) {
		stage.getScene().setRoot(root);

		stage.show();
	}

	public void createBomb(boolean edit) {
		try {
			sceneLoader.loadCreateBomb(!edit);
			setScene("create_bomb");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void chooseModule() {
		try {
			sceneLoader.loadChooseModule();
			setScene("choose_module");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
