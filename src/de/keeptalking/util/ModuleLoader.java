package de.keeptalking.util;

import com.sun.org.apache.xpath.internal.operations.Mod;
import de.keeptalking.main.bomb.Module;
import de.keeptalking.modules.AdjacentLetters;
import de.keeptalking.modules.ComplicatedWires;
import de.keeptalking.modules.Laundry;
import de.keeptalking.modules.TheButton;

import java.lang.reflect.InvocationTargetException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ModuleLoader {

	private List<Module> modules = new ArrayList<>();

	public Module getModule(Class<? extends Module> type) {
		for (Module mod : modules) {
			if (mod.getClass().getName().equals(type.getName())) {
				return mod;
			}
		}
		return null;
	}

	public Module getModulebyName(String name) {
		for (Module mod : modules) {
			if (mod.getClass().getSimpleName().equals(name.replaceAll(" ", ""))) {
				return mod;
			}
		}
		return null;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void registerModule(Class<? extends Module> type) {
		modules.add(load(type));
		modules.sort(Comparator.comparing(Module::toString));
	}

	private <T extends Module> T load(Class<T> type) {
		try {
			return type.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void registerModules() {
		registerModule(TheButton.class);
		registerModule(ComplicatedWires.class);
		registerModule(Laundry.class);
		registerModule(AdjacentLetters.class);
	}
}
