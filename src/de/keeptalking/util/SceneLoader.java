package de.keeptalking.util;

import de.keeptalking.main.KTaNEMain;
import de.keeptalking.main.bomb.Bomb;
import de.keeptalking.main.bomb.Indicator;
import de.keeptalking.main.bomb.Module;
import de.keeptalking.main.bomb.Port;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class SceneLoader {

	private KTaNEMain instance = KTaNEMain.instance;

	public void loadCreateBomb(boolean isNew) throws IOException {
		Bomb bomb = instance.bomb;
		Parent root = FXMLLoader.load(getClass().getResource("/de/keeptalking/create_bomb.fxml"));
		ObservableList ls = root.getChildrenUnmodifiable();

		TextField serialnum = (TextField) ls.get(16);
		TextField moduleAmount = (TextField) ls.get(17);

		if(!isNew){
			serialnum.setText(bomb.getSerial());
			if (bomb.getTotalModules() != 0) {
				moduleAmount.setText(String.valueOf(bomb.getTotalModules()));
			}
		}

		moduleAmount.textProperty().addListener((observable, oldValue, newValue) -> {
			for (char c : newValue.toCharArray()) {
				if (!Character.isDigit(c)) {
					moduleAmount.setText(oldValue);
					return;
				}
			}
		});

		serialnum.textProperty().addListener((observable, oldValue, newValue) -> {
			if(newValue.length() > 6) {
				serialnum.setText(oldValue);
			} else {
				serialnum.setText(newValue.toUpperCase());
			}
		});

		Pane batteries = (Pane) ls.get(0);
		AtomicInteger battery = new AtomicInteger();
		if(!isNew) battery.set(bomb.getBatteries());
		Button batteryAdd = (Button) batteries.getChildren().get(0);
		Button batterySub = (Button) batteries.getChildren().get(1);
		TextField batteryTex = (TextField) batteries.getChildren().get(2);
		batteryTex.setText("Batteries: " + battery);

		Pane holders = (Pane) ls.get(1);
		AtomicInteger holder = new AtomicInteger();
		if(!isNew) holder.set(bomb.getHolders());
		Button holderAdd = (Button) holders.getChildren().get(0);
		Button holderSub = (Button) holders.getChildren().get(1);
		TextField holderTex = (TextField) holders.getChildren().get(2);
		holderTex.setText("Holders: " + holder);

		batteryAdd.setOnAction(event -> {
			battery.getAndIncrement();
			batteryTex.setText("Batteries: " + battery);

			if (battery.get() == 0) {
				holder.set(0);
				holderTex.setText("Holders: " + holder);
			} else if (battery.get() == 1) {
				holder.set(1);
				holderTex.setText("Holders: " + holder);
			} else if (battery.get() > holder.get() * 2) {
				/* Round up */
				holder.set((int) (battery.get() / 2 + 1));
				holderTex.setText("Holders: " + holder);
			}
		});

		batterySub.setOnAction(event -> {
			if(battery.get() - 1 < 0) return;
			battery.getAndDecrement();
			batteryTex.setText("Batteries: " + battery);

			if (battery.get() < holder.get()) {
				holder.set(battery.get());
				holderTex.setText("Holders: " + holder);
			}
		});

		holderAdd.setOnAction(event -> {
			holder.getAndIncrement();
			holderTex.setText("Holders: " + holder);

			if (holder.get() > battery.get()) {
				battery.set(holder.get());
				batteryTex.setText("Batteries: " + battery);
			}
		});

		holderSub.setOnAction(event -> {
			if(holder.get() - 1 < 0) return;
			holder.getAndDecrement();
			holderTex.setText("Holders: " + holder);

			if (holder.get() == 0) {
				battery.set(0);
				batteryTex.setText("Batteries: " + battery);
			} else if (holder.get() * 2 < battery.get()) {
				battery.set(holder.get() * 2);
				batteryTex.setText("Batteries: " + battery);
			}
		});

		Pane lits = (Pane) ls.get(2);
		CheckBox litSND = (CheckBox) lits.getChildren().get(0);
		CheckBox litCLR = (CheckBox) lits.getChildren().get(1);
		CheckBox litCAR = (CheckBox) lits.getChildren().get(2);
		CheckBox litIND = (CheckBox) lits.getChildren().get(3);
		CheckBox litFRQ = (CheckBox) lits.getChildren().get(4);
		CheckBox litSIG = (CheckBox) lits.getChildren().get(5);
		CheckBox litNSA = (CheckBox) lits.getChildren().get(6);
		CheckBox litMSA = (CheckBox) lits.getChildren().get(7);
		CheckBox litTRN = (CheckBox) lits.getChildren().get(8);
		CheckBox litBOB = (CheckBox) lits.getChildren().get(9);
		CheckBox litFRK = (CheckBox) lits.getChildren().get(10);

		if(!isNew) {
			litSND.setSelected(bomb.indicatorIsLit(Indicator.SND));
			litCLR.setSelected(bomb.indicatorIsLit(Indicator.CLR));
			litCAR.setSelected(bomb.indicatorIsLit(Indicator.CAR));
			litIND.setSelected(bomb.indicatorIsLit(Indicator.IND));
			litFRQ.setSelected(bomb.indicatorIsLit(Indicator.FRQ));
			litSIG.setSelected(bomb.indicatorIsLit(Indicator.SIG));
			litNSA.setSelected(bomb.indicatorIsLit(Indicator.NSA));
			litMSA.setSelected(bomb.indicatorIsLit(Indicator.MSA));
			litTRN.setSelected(bomb.indicatorIsLit(Indicator.TRN));
			litBOB.setSelected(bomb.indicatorIsLit(Indicator.BOB));
			litFRK.setSelected(bomb.indicatorIsLit(Indicator.FRK));
		}

		Pane unlits = (Pane) ls.get(3);
		CheckBox unlitSND = (CheckBox) unlits.getChildren().get(0);
		CheckBox unlitCLR = (CheckBox) unlits.getChildren().get(1);
		CheckBox unlitCAR = (CheckBox) unlits.getChildren().get(2);
		CheckBox unlitIND = (CheckBox) unlits.getChildren().get(3);
		CheckBox unlitFRQ = (CheckBox) unlits.getChildren().get(4);
		CheckBox unlitSIG = (CheckBox) unlits.getChildren().get(5);
		CheckBox unlitNSA = (CheckBox) unlits.getChildren().get(6);
		CheckBox unlitMSA = (CheckBox) unlits.getChildren().get(7);
		CheckBox unlitTRN = (CheckBox) unlits.getChildren().get(8);
		CheckBox unlitBOB = (CheckBox) unlits.getChildren().get(9);
		CheckBox unlitFRK = (CheckBox) unlits.getChildren().get(10);

		if(!isNew) {
			unlitSND.setSelected(bomb.indicatorIsUnlit(Indicator.SND));
			unlitCLR.setSelected(bomb.indicatorIsUnlit(Indicator.CLR));
			unlitCAR.setSelected(bomb.indicatorIsUnlit(Indicator.CAR));
			unlitIND.setSelected(bomb.indicatorIsUnlit(Indicator.IND));
			unlitFRQ.setSelected(bomb.indicatorIsUnlit(Indicator.FRQ));
			unlitSIG.setSelected(bomb.indicatorIsUnlit(Indicator.SIG));
			unlitNSA.setSelected(bomb.indicatorIsUnlit(Indicator.NSA));
			unlitMSA.setSelected(bomb.indicatorIsUnlit(Indicator.MSA));
			unlitTRN.setSelected(bomb.indicatorIsUnlit(Indicator.TRN));
			unlitBOB.setSelected(bomb.indicatorIsUnlit(Indicator.BOB));
			unlitFRK.setSelected(bomb.indicatorIsUnlit(Indicator.FRK));
		}

		litSND.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitSND.setSelected(false);
			}
		});
		unlitSND.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litSND.setSelected(false);
			}
		});

		litCLR.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitCLR.setSelected(false);
			}
		});
		unlitCLR.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litCLR.setSelected(false);
			}
		});

		litCAR.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitCAR.setSelected(false);
			}
		});
		unlitCAR.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litCAR.setSelected(false);
			}
		});

		litIND.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitIND.setSelected(false);
			}
		});
		unlitIND.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litIND.setSelected(false);
			}
		});

		litFRQ.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitFRQ.setSelected(false);
			}
		});
		unlitFRQ.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litFRQ.setSelected(false);
			}
		});

		litSIG.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitSIG.setSelected(false);
			}
		});
		unlitSIG.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litSIG.setSelected(false);
			}
		});

		litNSA.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitNSA.setSelected(false);
			}
		});
		unlitNSA.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litNSA.setSelected(false);
			}
		});

		litMSA.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitMSA.setSelected(false);
			}
		});
		unlitMSA.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litMSA.setSelected(false);
			}
		});

		litTRN.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitTRN.setSelected(false);
			}
		});
		unlitTRN.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litTRN.setSelected(false);
			}
		});

		litBOB.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitBOB.setSelected(false);
			}
		});
		unlitBOB.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litBOB.setSelected(false);
			}
		});

		litFRK.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				unlitFRK.setSelected(false);
			}
		});
		unlitFRK.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				litFRK.setSelected(false);
			}
		});

		Pane dvis = (Pane) ls.get(7);
		AtomicInteger dvi = new AtomicInteger();
		if(!isNew) dvi.set(bomb.portCountOf(Port.DVI));
		Button dviAdd = (Button) dvis.getChildren().get(1);
		Button dviSub = (Button) dvis.getChildren().get(0);
		TextField dviTex = (TextField) dvis.getChildren().get(2);
		dviTex.setText("DVI-D: " + dvi);

		dviAdd.setOnAction(event -> {
			dvi.getAndIncrement();
			dviTex.setText("DVI-D: " + dvi);
		});

		dviSub.setOnAction(event -> {
			if(dvi.get() - 1 < 0) return;
			dvi.getAndDecrement();
			dviTex.setText("DVI-D: " + dvi);
		});

		Pane parallels = (Pane) ls.get(8);
		AtomicInteger parallel = new AtomicInteger();
		if(!isNew) parallel.set(bomb.portCountOf(Port.Parallel));
		Button parallelAdd = (Button) parallels.getChildren().get(0);
		Button parallelSub = (Button) parallels.getChildren().get(1);
		TextField parallelTex = (TextField) parallels.getChildren().get(2);
		parallelTex.setText("Parallel: " + parallel);

		parallelAdd.setOnAction(event -> {
			parallel.getAndIncrement();
			parallelTex.setText("Parallel: " + parallel);
		});

		parallelSub.setOnAction(event -> {
			if(parallel.get() - 1 < 0) return;
			parallel.getAndDecrement();
			parallelTex.setText("Parallel: " + parallel);
		});

		Pane ps2s = (Pane) ls.get(9);
		AtomicInteger ps2 = new AtomicInteger();
		if(!isNew) ps2.set(bomb.portCountOf(Port.PS2));
		Button ps2Add = (Button) ps2s.getChildren().get(0);
		Button ps2Sub = (Button) ps2s.getChildren().get(1);
		TextField ps2Tex = (TextField) ps2s.getChildren().get(2);
		ps2Tex.setText("PS/2: " + ps2);

		ps2Add.setOnAction(event -> {
			ps2.getAndIncrement();
			ps2Tex.setText("PS/2: " + ps2);
		});

		ps2Sub.setOnAction(event -> {
			if(ps2.get() - 1 < 0) return;
			ps2.getAndDecrement();
			ps2Tex.setText("PS/2: " + ps2);
		});

		Pane rj45s = (Pane) ls.get(10);
		AtomicInteger rj45 = new AtomicInteger();
		if(!isNew) rj45.set(bomb.portCountOf(Port.RJ45));
		Button rj45Add = (Button) rj45s.getChildren().get(0);
		Button rj45Sub = (Button) rj45s.getChildren().get(1);
		TextField rj45Tex = (TextField) rj45s.getChildren().get(2);
		rj45Tex.setText("RJ-45: " + rj45);

		rj45Add.setOnAction(event -> {
			rj45.getAndIncrement();
			rj45Tex.setText("RJ-45: " + rj45);
		});

		rj45Sub.setOnAction(event -> {
			if(rj45.get() - 1 < 0) return;
			rj45.getAndDecrement();
			rj45Tex.setText("RJ-45: " + rj45);
		});

		Pane serials = (Pane) ls.get(11);
		AtomicInteger serial = new AtomicInteger();
		if(!isNew) serial.set(bomb.portCountOf(Port.Serial));
		Button serialAdd = (Button) serials.getChildren().get(0);
		Button serialSub = (Button) serials.getChildren().get(1);
		TextField serialTex = (TextField) serials.getChildren().get(2);
		serialTex.setText("Serial: " + serial);

		serialAdd.setOnAction(event -> {
			serial.getAndIncrement();
			serialTex.setText("Serial: " + serial);
		});

		serialSub.setOnAction(event -> {
			if(serial.get() - 1 < 0) return;
			serial.getAndDecrement();
			serialTex.setText("Serial: " + serial);
		});

		Pane rcas = (Pane) ls.get(12);
		AtomicInteger rca = new AtomicInteger();
		if(!isNew) rca.set(bomb.portCountOf(Port.RCA));
		Button rcaAdd = (Button) rcas.getChildren().get(0);
		Button rcaSub = (Button) rcas.getChildren().get(1);
		TextField rcaTex = (TextField) rcas.getChildren().get(2);
		rcaTex.setText("RCA: " + rca);

		rcaAdd.setOnAction(event -> {
			rca.getAndIncrement();
			rcaTex.setText("RCA: " + rca);
		});

		rcaSub.setOnAction(event -> {
			if(rca.get() - 1 < 0) return;
			rca.getAndDecrement();
			rcaTex.setText("RCA: " + rca);
		});

		Pane plates = (Pane) ls.get(13);
		AtomicInteger plate = new AtomicInteger();
		if(!isNew) plate.set(bomb.getPlates());
		Button plateAdd = (Button) plates.getChildren().get(0);
		Button plateSub = (Button) plates.getChildren().get(1);
		TextField plateTex = (TextField) plates.getChildren().get(2);
		plateTex.setText("Plates: " + plate);

		plateAdd.setOnAction(event -> {
			plate.getAndIncrement();
			plateTex.setText("Plates: " + plate);
		});

		plateSub.setOnAction(event -> {
			if(plate.get() - 1 < 0) return;
			plate.getAndDecrement();
			plateTex.setText("Plates: " + plate);
		});

		Pane empties = (Pane) ls.get(14);
		AtomicInteger empty = new AtomicInteger();
		if(!isNew) empty.set(bomb.getEmpties());
		Button emptyAdd = (Button) empties.getChildren().get(0);
		Button emptySub = (Button) empties.getChildren().get(1);
		TextField emptyTex = (TextField) empties.getChildren().get(2);
		emptyTex.setText("Empties: " + empty);

		emptyAdd.setOnAction(event -> {
			empty.getAndIncrement();
			emptyTex.setText("Empties: " + empty);
		});

		emptySub.setOnAction(event -> {
			if(empty.get() - 1 < 0) return;
			empty.getAndDecrement();
			emptyTex.setText("Empties: " + empty);
		});

		Button submit = (Button) ls.get(15);
		submit.setOnAction(event -> {
			boolean allowed = true;

			/* Test if bomb settings are allowed */
			if (moduleAmount.getText().isEmpty()) {
				addErrorMark(moduleAmount);
				allowed = false;
			}

			if (!allowed) return;

			bomb.setSerial(serialnum.getText());
			bomb.setBatteries(battery.get());
			bomb.setHolders(holder.get());
			bomb.setPlates(plate.get());
			bomb.setEmpties(empty.get());

			bomb.setStrikes(0);
			bomb.setSolvedModules(0);

			try {
				bomb.setTotalModules(Integer.parseInt(moduleAmount.getText()));
			} catch (NumberFormatException ignored){

			}
			
			if(litSND.isSelected()) bomb.addLit(Indicator.SND);
			if(litCLR.isSelected()) bomb.addLit(Indicator.CLR);
			if(litCAR.isSelected()) bomb.addLit(Indicator.CAR);
			if(litIND.isSelected()) bomb.addLit(Indicator.IND);
			if(litFRQ.isSelected()) bomb.addLit(Indicator.FRQ);
			if(litSIG.isSelected()) bomb.addLit(Indicator.SIG);
			if(litNSA.isSelected()) bomb.addLit(Indicator.NSA);
			if(litMSA.isSelected()) bomb.addLit(Indicator.MSA);
			if(litTRN.isSelected()) bomb.addLit(Indicator.TRN);
			if(litFRK.isSelected()) bomb.addLit(Indicator.FRK);
			if(litBOB.isSelected()) bomb.addLit(Indicator.BOB);

			if(unlitSND.isSelected()) bomb.addUnlit(Indicator.SND);
			if(unlitCLR.isSelected()) bomb.addUnlit(Indicator.CLR);
			if(unlitCAR.isSelected()) bomb.addUnlit(Indicator.CAR);
			if(unlitIND.isSelected()) bomb.addUnlit(Indicator.IND);
			if(unlitFRQ.isSelected()) bomb.addUnlit(Indicator.FRQ);
			if(unlitSIG.isSelected()) bomb.addUnlit(Indicator.SIG);
			if(unlitNSA.isSelected()) bomb.addUnlit(Indicator.NSA);
			if(unlitMSA.isSelected()) bomb.addUnlit(Indicator.MSA);
			if(unlitTRN.isSelected()) bomb.addUnlit(Indicator.TRN);
			if(unlitFRK.isSelected()) bomb.addUnlit(Indicator.FRK);
			if(unlitBOB.isSelected()) bomb.addUnlit(Indicator.BOB);

			bomb.getPorts().clear();

			for(int dvii = dvi.get(); dvii > 0; dvii--) {
				bomb.addPort(Port.DVI);
			}

			for(int paralleli = parallel.get(); paralleli > 0; paralleli--) {
				bomb.addPort(Port.Parallel);
			}

			for(int ps2i = ps2.get(); ps2i > 0; ps2i--) {
				bomb.addPort(Port.PS2);
			}

			for(int rj45i = rj45.get(); rj45i > 0; rj45i--) {
				bomb.addPort(Port.RJ45);
			}

			for(int seriali = serial.get(); seriali > 0; seriali--) {
				bomb.addPort(Port.Serial);
			}

			for(int rcai = rca.get(); rcai > 0; rcai--) {
				bomb.addPort(Port.RCA);
			}

			instance.chooseModule();
		});

		instance.getScenes().remove("create_bomb");
		instance.getScenes().put("create_bomb", root);
	}

	public void loadChooseModule() throws IOException {
		Bomb bomb = instance.bomb;
		Parent root = FXMLLoader.load(getClass().getResource("/de/keeptalking/choose_module.fxml"));
		ObservableList ls = root.getChildrenUnmodifiable();
		KTaNEMain main = KTaNEMain.instance;

		AtomicReference<List<Module>> modules = new AtomicReference<>(new ArrayList<>(main.getModuleLoader().getModules()));

		ListView list = (ListView) ls.get(0);
		TextField field = (TextField) ls.get(1);
		Button editBomb = (Button) ls.get(2);
		Button newBomb = (Button) ls.get(3);

		Pane paneSolved = (Pane) ls.get(4);
		AtomicInteger solved = new AtomicInteger(bomb.getSolvedModules());
		Button removeSolved = (Button) paneSolved.getChildren().get(0);
		TextField textSolved = (TextField) paneSolved.getChildren().get(1);
		Button addSolved = (Button) paneSolved.getChildren().get(2);

		Pane paneStrikes = (Pane) ls.get(5);
		AtomicInteger strike = new AtomicInteger(bomb.getStrikes());
		Button removeStrike = (Button) paneStrikes.getChildren().get(0);
		TextField textStrike = (TextField) paneStrikes.getChildren().get(1);
		Button addStrike = (Button) paneStrikes.getChildren().get(2);

		Text modulesDisplay = (Text) ls.get(6);

		modulesDisplay.setText("Amount Of Modules: " + bomb.getTotalModules());

		editBomb.setOnAction(event -> instance.createBomb(true));
		newBomb.setOnAction(event -> instance.createBomb(false));

		textSolved.setText("Solved: " + solved);
		textStrike.setText("Strikes: " + strike);

		addSolved.setOnAction(event -> {
			if (solved.get() + 1 > bomb.getTotalModules()) return;
			solved.getAndIncrement();
			textSolved.setText("Solved: " + solved);
			bomb.setSolvedModules(solved.get());
		});

		removeSolved.setOnAction(event -> {
			if (solved.get() - 1 < 0) return;
			solved.getAndDecrement();
			textSolved.setText("Solved: " + solved);
			bomb.setSolvedModules(solved.get());
		});

		addStrike.setOnAction(event -> {
			strike.getAndIncrement();
			textStrike.setText("Strikes: " + strike);
			bomb.setStrikes(strike.get());
		});

		removeStrike.setOnAction(event -> {
			if (strike.get() - 1 < 0) return;
			strike.getAndDecrement();
			textStrike.setText("Strikes: " + strike);
			bomb.setStrikes(strike.get());
		});

		for (Module mod : modules.get()) {
			list.getItems().add(mod.getName());
		}

		list.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				String module = list.getSelectionModel().getSelectedItem().toString();
				main.getModuleLoader().getModulebyName(module).openScene();
			}
		});

		field.textProperty().addListener((observable, oldValue, newValue) -> {
			modules.set(new ArrayList<>(main.getModuleLoader().getModules()));

			modules.get().removeIf(mod -> !mod.getName().toLowerCase().startsWith(newValue.toLowerCase()));
			list.getItems().clear();

			for (Module mod : modules.get()) {
				list.getItems().add(mod.getName());
			}
		});

		instance.getScenes().remove("choose_module");
		instance.getScenes().put("choose_module", root);
	}

	public void addErrorMark(TextField tf) {
		tf.setStyle(
				"-fx-focus-color: #d35244;" +
				"-fx-faint-focus-color: #d3524422;" +
				"-fx-highlight-fill: -fx-accent;" +
				"-fx-highlight-text-fill: white;" +
				"-fx-background-color:" +
				"-fx-focus-color," +
				"-fx-control-inner-background," +
				"-fx-faint-focus-color," +
				"linear-gradient(from 0px 0px to 0px 5px, derive(-fx-control-inner-background, -9%), -fx-control-inner-background);" +
				"-fx-background-insets: -0.2, 1, -1.4, 3;" +
				"-fx-background-radius: 3, 2, 4, 0;"
		);
	}


}
