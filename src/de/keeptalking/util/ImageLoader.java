package de.keeptalking.util;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class ImageLoader {

	public void setImage(ImageView view, String imgName) {
		System.out.println("Loading image /de/keeptalking/res/" + imgName + ".png");
		Image image = new Image("de/keeptalking/res/" + imgName + ".png");

		double w = 0;
		double h = 0;

		double ratioX = view.getFitWidth() / image.getWidth();
		double ratioY = view.getFitHeight() / image.getHeight();

		double reducCoeff = 0;
		if(ratioX >= ratioY) {
			reducCoeff = ratioY;
		} else {
			reducCoeff = ratioX;
		}

		w = image.getWidth() * reducCoeff;
		h = image.getHeight() * reducCoeff;

		view.setX((view.getFitWidth() - w) / 2);
		view.setY((view.getFitHeight() - h) / 2);

		view.setImage(image);
		view.setFitWidth(100);
		view.setPreserveRatio(true);
		view.setSmooth(true);
		view.setCache(true);
	}

}
